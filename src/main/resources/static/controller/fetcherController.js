import { displayExtract } from "../view/fetcherView.js";

const currentUrl = window.location.href;
const backUrl = currentUrl + "api/wiki/fetch";
let expression;
let extract;

(() => addActions())();

function updateExpression(event) {
    expression = event.target.value;
}

function fetchExtract() {
    displayExtract("Recherche...");
    fetch(backUrl + "/extract/" + expression)
        .then(response => response.text())
        .then(extract => {
            if (extract !== "") {
                displayExtract(extract);
            }
        }); 
}

function addActions() {
    const expressionButton = document.getElementById("expression");
    expressionButton.addEventListener("change", event => updateExpression(event));
    const fetchButton = document.getElementById("button");
    fetchButton.addEventListener("click", () => fetchExtract());
}
