package fr.eql.akatz.wiki.extract.fetcher.controller.rest;

import fr.eql.akatz.wiki.extract.fetcher.service.FetcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = { "*" })
@RequestMapping("api/wiki/fetch")
public class FetcherRestController {

    /** Injecté par le setter. */
    FetcherService fetcherService;

    @GetMapping("/extract/{expression}")
    public String fetchExtract(@PathVariable String expression) {
        return fetcherService.fetchExtract(expression);
    }

    /// Setters ///
    @Autowired
    public void setFetcherService(FetcherService fetcherService) {
        this.fetcherService = fetcherService;
    }
}
