package fr.eql.akatz.wiki.extract.fetcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//toto
@SpringBootApplication
public class WikiExtractFetcherApplication {

	public static void main(String[] args) {
		SpringApplication.run(WikiExtractFetcherApplication.class, args);
	}

}
