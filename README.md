# Wiki Exctract Fetcher

## Vue glogbale du pipeline

Globalement le pipeline est décomposé en deux :

- Un pipeline de CI s'occupant :
  - de construire le "jar spring boot" encapsulé dans une image OCI
  - de résaliser des tests (unitaires, intégrations, sast et scan de conteneurs)
  - de publier sur la repository de container/tagger les images en fonction du contexte
  - de déclancher le pipeline de CD
- Un pipeline de CD s'occupant :
  - de valoriser les templates de descripteurs de déploiements en fonction de l'environnement cible
  - de déployer sur un cluster kubernetes l'application
  
le pipeline va avoir un comportement un peu différent en fonction du "trigger"

- Sur un commit sur une branche liée à une MR :
  - Déclencher la full CI (build, test, publish)
  - déployer dans le namespace "dev" l'application disponible à l'url : http://ip.du.cluster/wikiapp-NOM_DE_LA_BRANCHE (l'url est aussi disponible dans les "environnements" GitLab)

- Sur un commit sur la branche principale :
  - Déclencher la full CI (build, test, publish)
  - tag l'image avec "dev-latest" et "dev-main-latest"
  - déployer dans le namespace "dev" l'application disponible à l'url : http://ip.du.cluster/wikiapp-dev
  
- Sur un tag :
  - Retag l'image crée précédement (sans relancer de build ! "Promotion de build") avec la valeur du tag git
  - déployer l'image "tag" dans le namespace "dev" l'application disponible à l'url : http://ip.du.cluster/wikiapp-dev
  - déployer l'image "tag" dans le namespace "ppd" l'application disponible à l'url : http://ip.du.cluster/wikiapp-ppd
  - déployer l'image "tag" dans le namespace "prod" l'application disponible à l'url : http://ip.du.cluster/wikiapp-prod

## Mise en place de la cd

Pour le déploiement il y a besoin d'un cluster kubernetes configuré pour acceuilir l'application

1. Dans GCP créer un nouveau projet/cluster dans le projet
2. Se connecter au cluster (via gcloud par exemple)
3. créer les namespaces dev, ppd et prod via la commande :

   ```bash
   kubectl apply -f .infra/namespaces
   ```

4. Créer le namesapce pour traefik (qui permettra de router le trafic de l'extérieur vers l'intérieur du cluster) via la commande :

   ```bash
   kubectl apply -f .infra/traefik/namespace.yaml
   ```

5. Déployer traefik sur le cluster

   ```bash
   helm repo add traefik https://traefik.github.io/charts
   helm repo update
   helm install --namespace=traefik-v2 traefik traefik/traefik
   ```

6. Rendre le dashboard traefik accessible :  

   ```bash
   kubectl apply -f .infra/traefik/dashboard.yaml
   ```

7. Déployer un "agent" gitlab sur le cluster :

   - Se rendre dans le menu "operate/Kubernetes clusters"
   - Supprimer tout agent déjà présent
   - Recharger la page et cliquer sur "connect a cluser"
   - slectionner "gcp" puis suivre les instruction

8. Dans le fichier .gitlab/ci/deploy.gitlab-ci.yml modifier l'IP des applications avec l'IP publique du service "Traefik" accessible dans la "console" gcp sur le web

Et voilà vous pouvez utiliser le pipeline :)
