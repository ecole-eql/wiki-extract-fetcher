FROM openjdk:11.0-jre-slim

RUN useradd -ms /bin/bash wikiapp
COPY --chown=wikiapp:wikiapp ./target/*.jar /home/wikiapp/*.jar

USER wikiapp
WORKDIR /home/wikiapp

EXPOSE 8080
ENTRYPOINT [ "/bin/sh", "-c", "java -jar /home/wikiapp/*.jar" ]
